import * as config from "./config";
import { roomCreationHandler, joinRoomHandler, userLeftRoom, uploadRooms } from "./rooms";
import { toggleReadyStatus } from "./gamePreparation";
import { startGame, enterLetter } from "./gameProcess";

const disconnectHandler = (io, username) => {
  const rooms = io.rooms;
  if (rooms.size === 0) {
    return;
  }
  const userRooms = Object.keys(Object.fromEntries(rooms.entries())).filter(room => rooms.get(room).users.has(username));
  if (userRooms.length === 0) {
    return;
  }
  const room = userRooms[0];
  userLeftRoom(io, username, room);

}

export default io => {
  io.rooms = new Map();
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    const connectedUsernames = Object.values(io.sockets.sockets).map(el => el.username).filter(el => el);

  
    if (connectedUsernames.indexOf(username) >= 0) {
      console.log('Error!');
      socket.emit("REAUTHORIZATION_ERROR", username);
    }
    else {
      socket.username = username;
    }

    uploadRooms(io, socket);
    
    socket.on('CREATING_ROOM', title => roomCreationHandler(io, socket, title));

    socket.on('USER_LEFT_ROOM', (username, title) => userLeftRoom(io, username, title));

    socket.on('JOIN_ROOM', room => joinRoomHandler(io, socket, room));

    socket.on('TOGGLE_READY_STATUS', () => toggleReadyStatus(io, socket));

    socket.on('disconnect', () => disconnectHandler(io, username));

    socket.on('ENTER_LETTER', obj => enterLetter(io, obj));

  });
}