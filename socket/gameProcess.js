import { SECONDS_FOR_GAME, MAXIMUM_USERS_FOR_ONE_ROOM } from "./config";
import { updateTimer } from "./gamePreparation";

export const startGame = async (io, room) => {
    io[room].isTimerStarted = false;
    io[room].isGameStarted = true;
    io.in(room).emit("GAME_START");
    await updateTimer(io, room, SECONDS_FOR_GAME, 'game-timer');
    const rate = Object.entries(io[room].statistics)
        .filter(([,v]) => v.currSize !== v.textSize)
        .sort(([, v1],[,v2]) => v2.currSize-v1.currSize)
        .map(([k,]) => k);
    io[room].rating.push(...rate);
    finishGame(io, room);

}

export const enterLetter = (io, { username, currSize, textSize }) => {
    let room = null;
    io.rooms.forEach((val, roomName) => {
        if (val.users.has(username)) {
            room = roomName;
        }
    });

    io[room].statistics[username] = { currSize, textSize };

    if (currSize === textSize) {
        io[room].rating.push(username);
        if (io[room].rating.length === Object.keys(io[room].readyStatus).length) {
            finishGame(io, room);
            return;
        }
    }

    io.in(room).emit("UPDATE_PROGRESS_BAR", username, currSize === textSize ? 100 : currSize / textSize * 100);
}

export const finishGame = (io, room) => {
    clearInterval(io[room].interval);
    io[room].isTimerStarted = false;
    io[room].isGameStarted = false;
    if ( !(io.rooms.get(room).users.size === MAXIMUM_USERS_FOR_ONE_ROOM)) {
        io.rooms.get(room).visible = true;
        io.emit("SHOW_ROOM", room);
    }
    
    Object.keys(io[room].readyStatus).forEach(user => {
        const userId = Object.values(Object.entries(io.sockets.sockets).filter(([id, val]) => val.username === user)[0])[0];
        io[room].readyStatus[user] = false;
        //io.to(room).emit("FINISH_GAME", io[room].rating, user);
        io.to(userId).emit("SHOW_RESULTS", io[room].rating);
        io.to(room).emit("TOGGLE_INDICATORS", user, false);
        io.to(room).emit("UPDATE_PROGRESS_BAR", user, 0);
    });
    io[room].statistics = {};
    io[room].rating = [];
}