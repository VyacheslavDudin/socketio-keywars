export const roomCreationErrorHandler = title => alert(`Error! Room ${title} is already exists`);

export const joinRoomErrorHandler = title => {
    alert(`Error! Room \"${title}\" is already closed!`);
};

export const reauthorizationErrorHandler = username => {
    alert(`Error! User with username ${username} is already connected!`);
    sessionStorage.removeItem('username');
    window.location.replace("/login");
}