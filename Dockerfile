FROM node:12

COPY public ./public
COPY routes ./routes
COPY socket ./socket
COPY .babelrc ./
COPY config.js ./
COPY data.js ./
COPY package.json ./
COPY server.js ./
COPY yarn.lock ./
RUN npm install

EXPOSE $PORT
CMD ["npm", "start"]